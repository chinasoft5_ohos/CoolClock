# CoolClock

## 项目介绍
- 项目名称：CoolClock
- 所属系列：openharmony的第三方组件适配移植
- 功能：滚轮控件，基于滑动列表实现，可以自定义样式
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：master分支
## 效果演示
 ![Image text](./gifs/CoolClock.gif)
## 安装教程
该组件是纯app，不包含library，没有上传maven仓库
- 下载app的hap包CoolClock.hap(位于：https://gitee.com/chinasoft5_ohos/CoolClock/releases/1.0.0)
在sdk6，DevEco Studio2.2 Beta1下项目可直接运行；如无法运行，
删除项目.gradle,.idea,build,gradle,build.gradle文件，并格根据自己的版本创建新项目，
将新项目对应的文件复制到根目录下

## 使用说明

## 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

## 待实现
- 卸载应用本身
- 遮挡光线传感器锁屏
- 天气功能

## 版本迭代
- 1.0.0
- 0.0.1-SNAPSHOT

## 版权和许可信息

GPL-3.0










