/*
 * Copyright (C) 2016 venshine.cn@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wx.wheelview.graphics;

import com.wx.wheelview.common.WheelConstants;
import com.wx.wheelview.widget.WheelView;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;

/**
 * common滚轮样式
 *
 * @author venshine
 */
public class CommonDrawable extends WheelDrawable {
    /**
     * 阴影色值
     */
    private static final RgbColor[] SHADOWS_COLORS = {
            /*new RgbColor(0xFF111111),
            new RgbColor(0x00AAAAAA),
            new RgbColor(0x00AAAAAA)*/
            new RgbColor(17, 17, 17),
            new RgbColor(170, 170, 170, 0),
            new RgbColor(170, 170, 170, 0)
    };

    private Paint mCommonBgPaint;
    private Paint mCommonPaint;
    private Paint mCommonDividerPaint;
    private Paint mCommonBorderPaint;

    private int mWheelSize;
    private int mItemH;

    /**
     * 顶部阴影
     */
    private ShapeElement mTopShadow = new ShapeElement();
    /**
     * 底部阴影
     */
    private ShapeElement mBottomShadow = new ShapeElement();

    /**
     * CommonDrawable instance
     *
     * @param canvas 画布
     * @param width 宽
     * @param height 高
     * @param style 滚轮风格
     * @param wheelSize 滚轮个数
     * @param itemH item高度
     */
    CommonDrawable(Canvas canvas, int width, int height, WheelView.WheelViewStyle style, int wheelSize,
            int itemH) {
        super(canvas, width, height, style, false);
        mWheelSize = wheelSize;
        mItemH = itemH;
        init(canvas);
    }

    private void init(Canvas canvas) {
        mCommonBgPaint = new Paint();
        mCommonBgPaint.setColor(
                new Color(mStyle.backgroundColor != -1 ? mStyle.backgroundColor : WheelConstants.WHEEL_SKIN_COMMON_BG));

        mCommonPaint = new Paint();
        mCommonPaint.setColor(new Color(WheelConstants.WHEEL_SKIN_COMMON_COLOR));

        mCommonDividerPaint = new Paint();
        mCommonDividerPaint.setColor(new Color(WheelConstants.WHEEL_SKIN_COMMON_DIVIDER_COLOR));
        mCommonDividerPaint.setStrokeWidth(2);

        mCommonBorderPaint = new Paint();
        mCommonBorderPaint.setStrokeWidth(6);
        mCommonBorderPaint.setColor(new Color(WheelConstants.WHEEL_SKIN_COMMON_BORDER_COLOR));

        mTopShadow.setGradientOrientation(Orientation.TOP_TO_BOTTOM);
        mTopShadow.setRgbColors(SHADOWS_COLORS);

        mBottomShadow.setGradientOrientation(Orientation.BOTTOM_TO_TOP);
        mBottomShadow.setRgbColors(SHADOWS_COLORS);
        drawToCanvas(canvas);
    }

    @Override
    public void drawToCanvas(Canvas canvas) {
        super.drawToCanvas(canvas);
        /**
         * draw background
         */
        canvas.drawRect(0, 0, mWidth, mHeight, mCommonBgPaint);

        /**
         * draw select border
         */
        if (mItemH != 0) {
            int size = mWheelSize >> 1;
            canvas.drawRect(0, mItemH * size, mWidth, mItemH
                    * (size + 1), mCommonPaint);
            canvas.drawLine(0, mItemH * size, mWidth, mItemH
                    * size, mCommonDividerPaint);
            canvas.drawLine(0, mItemH * (size + 1), mWidth, mItemH
                    * (size + 1), mCommonDividerPaint);

            /**
             * top, bottom
             */
//            mTopShadow.setBounds(0, 0, mWidth, mItemH);
//            mTopShadow.drawToCanvas(canvas);
//
//            mBottomShadow.setBounds(0, mHeight - mItemH, mWidth, mHeight);
//            mBottomShadow.drawToCanvas(canvas);

            /**
             * left,right
             */
            canvas.drawLine(0, 0, 0, mHeight, mCommonBorderPaint);
            canvas.drawLine(mWidth, 0, mWidth, mHeight, mCommonBorderPaint);
        }
    }
}
