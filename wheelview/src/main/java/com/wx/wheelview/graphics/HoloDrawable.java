/*
 * Copyright (C) 2016 venshine.cn@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wx.wheelview.graphics;

import com.wx.wheelview.common.WheelConstants;
import com.wx.wheelview.widget.WheelView;

import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;

/**
 * holo滚轮样式
 *
 * @author venshine
 */
public class HoloDrawable extends WheelDrawable {
    private Paint mHoloBgPaint;
    private Paint mHoloPaint;

    private int mWheelSize;
    private int mItemH;

    /**
     * HoloDrawable instance
     *
     * @param canvas 画布
     * @param width 宽
     * @param height 高
     * @param style 滚轮风格
     * @param wheelSize 滚轮个数
     * @param itemH item高度
     */
    HoloDrawable(Canvas canvas, int width, int height, WheelView.WheelViewStyle style, int wheelSize, int itemH) {
        super(canvas, width, height, style, false);
        mWheelSize = wheelSize;
        mItemH = itemH;
        init(canvas);
    }

    private void init(Canvas canvas) {
        mHoloBgPaint = new Paint();
        mHoloBgPaint.setColor(new Color(mStyle.backgroundColor != -1 ? mStyle.backgroundColor :
                WheelConstants.WHEEL_SKIN_HOLO_BG));

        mHoloPaint = new Paint();
        mHoloPaint.setStrokeWidth(mStyle.holoBorderWidth != -1 ? mStyle.holoBorderWidth : 3);
        mHoloPaint.setColor(new Color(
                mStyle.holoBorderColor != -1 ? mStyle.holoBorderColor : WheelConstants.WHEEL_SKIN_HOLO_BORDER_COLOR));
        drawToCanvas(canvas);
    }

    @Override
    public void drawToCanvas(Canvas canvas) {
        super.drawToCanvas(canvas);
        canvas.drawRect(0, 0, mWidth, mHeight, mHoloBgPaint);

        /**
         * draw select border
         */
        if (mItemH != 0) {
            int size = mWheelSize >> 1;
            canvas.drawLine(0, mItemH * size, mWidth, mItemH * size, mHoloPaint);
            canvas.drawLine(0, mItemH * (size + 1), mWidth, mItemH * (size + 1), mHoloPaint);
        }
    }
}
