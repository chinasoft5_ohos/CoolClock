/*
 * Copyright (C) 2016 venshine.cn@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wx.wheelview.graphics;

import com.wx.wheelview.common.WheelConstants;
import com.wx.wheelview.widget.WheelView;

import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;

/**
 * 滚轮样式
 *
 * @author venshine
 */
public class WheelDrawable extends ShapeElement {
    /**
     * 控件宽
     */
    protected int mWidth;
    /**
     * 控件高
     */
    protected int mHeight;
    /**
     * 滚轮风格
     */
    protected WheelView.WheelViewStyle mStyle;
    /**
     * 背景画笔
     */
    protected Paint mBgPaint;

    /**
     * WheelDrawable instance
     *
     * @param canvas 画布
     * @param width 宽
     * @param height 高
     * @param style 滚轮风格
     * @param isDrawToCanvas 是否主动执行drawToCanvas
     */
    protected WheelDrawable(Canvas canvas, int width, int height, WheelView.WheelViewStyle style, boolean isDrawToCanvas) {
        mWidth = width;
        mHeight = height;
        mStyle = style;
        init();
        if (isDrawToCanvas) {
            drawToCanvas(canvas);
        }
    }

    private void init() {
        mBgPaint = new Paint();
        mBgPaint.setColor(new Color(mStyle.backgroundColor != -1 ? mStyle.backgroundColor :
                WheelConstants.WHEEL_BG));
    }

    @Override
    public void drawToCanvas(Canvas canvas) {
        super.drawToCanvas(canvas);
        canvas.drawRect(0, 0, mWidth, mHeight, mBgPaint);
    }

    @Override
    public void setAlpha(int alpha) {
        super.setAlpha(alpha);
    }
}
