/*
 * Copyright (C) 2016 venshine.cn@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wx.wheelview.graphics;

import com.wx.wheelview.widget.WheelView;

import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;

/**
 * @author venshine
 */
public class DrawableFactory {
    /**
     * 创建ShapeElement对象
     * @param canvas 画布
     * @param skin 皮肤
     * @param width 宽
     * @param height 高
     * @param style 滚轮风格
     * @param wheelSize 滚轮个数
     * @param itemH item高度
     * @return ShapeElement对象
     */
    public static ShapeElement createDrawable(Canvas canvas, WheelView.Skin skin, int width, int height,
            WheelView.WheelViewStyle style, int wheelSize, int itemH) {
        if (skin.equals(WheelView.Skin.Common)) {
            return new CommonDrawable(canvas, width, height, style, wheelSize, itemH);
        } else if (skin.equals(WheelView.Skin.Holo)) {
            return new HoloDrawable(canvas, width, height, style, wheelSize, itemH);
        } else {
            return new WheelDrawable(canvas, width, height, style, true);
        }
    }
}
