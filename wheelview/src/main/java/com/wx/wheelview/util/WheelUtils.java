/*
 * Copyright (C) 2016 venshine.cn@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wx.wheelview.util;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Text;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import java.util.Collection;

/**
 * 滚轮工具类
 *
 * @author venshine
 */
public class WheelUtils {
    private static final String TAG = "WheelView";

    /**
     * 打印日志
     *
     * @param msg 消息内容
     */
    public static void log(String msg) {
        if (!TextUtils.isEmpty(msg)) {
            LogUtil.debug(TAG, msg);
        }
    }

    /**
     * 判断集合是否为空
     *
     * @param collection 集合
     * @param <V> 泛型
     * @return 是否为空
     */
    public static <V> boolean isEmpty(Collection<V> collection) {
        return (collection == null || collection.size() == 0);
    }

    /**
     * 遍历查找TextView
     *
     * @param view 对象
     * @return Text对象
     */
    public static Text findTextView(Component view) {
        if (view instanceof Text) {
            return (Text) view;
        } else {
            if (view instanceof ComponentContainer) {
                for (int i = 0; i < ((ComponentContainer) view).getChildCount(); i++) {
                    Text textView = findTextView(((ComponentContainer) view).getComponentAt(i));
                    if (textView != null) {
                        return textView;
                    }
                }
            }
            return null;
        }
    }

    /**
     * 去掉后缀
     *
     * @param dimens vp/fp值
     * @return float值
     */
    public static float parseDimension(String dimens) {
        float dimensFloat = 0f;
        String newDimens = dimens.replace("vp", "").replace("fp", "");
        try {
            dimensFloat = Float.parseFloat(newDimens);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return dimensFloat;
    }

    /**
     * vp转换到px
     *
     * @param context 上下文
     * @param dp dp值
     * @return px值
     */
    public static int dip2px(Context context, float dp) {
        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
        float scale = display.getAttributes().densityPixels;
        return (int) (dp * scale + 0.5d);
    }

    /**
     * sp转换到px
     *
     * @param context 上下文
     * @param sp sp值
     * @return px值
     */
    public static int sp2px(Context context, float sp) {
        float fontScale = DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes().scalDensity;
        return (int) (sp * fontScale + 0.5d);
    }

    /**
     * 获取屏幕宽度
     *
     * @param context 上下文
     * @return 屏幕宽度
     */
    public static int getDisplayWidthInPx(Context context) {
        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
        return display.getAttributes().width;
    }

    /**
     * 获取屏幕高度
     * @param context 上下文
     * @return 屏幕高度
     */
    public static int getDisplayHeightInPx(Context context) {
        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
        return display.getAttributes().height;
    }
}
