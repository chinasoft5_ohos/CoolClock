/*
 * Copyright (C) 2016 venshine.cn@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wx.wheelview.widget;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ScrollView;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

/**
 * 嵌套ScrollView
 *
 * @author venshine
 */
public class NestedScrollView extends ScrollView {
    /**
     * NestedScrollView instance
     *
     * @param context 上下文
     */
    public NestedScrollView(Context context) {
        this(context, null);
    }

    /**
     * NestedScrollView instance
     *
     * @param context 上下文
     * @param attrSet attrSet
     */
    public NestedScrollView(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    /**
     * NestedScrollView instance
     *
     * @param context 上下文
     * @param attrSet attrSet
     * @param styleName styleName
     */
    public NestedScrollView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init();
    }

    private void init() {
        setTouchEventListener(new TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                return false;
            }
        });
    }
}
