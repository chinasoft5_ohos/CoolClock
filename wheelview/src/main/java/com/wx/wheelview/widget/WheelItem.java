/*
 * Copyright (C) 2016 venshine.cn@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wx.wheelview.widget;

import com.wx.wheelview.common.WheelConstants;
import com.wx.wheelview.util.WheelUtils;

import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;

/**
 * 滚轮Item布局，包含图片和文本
 *
 * @author venshine
 */
public class WheelItem extends StackLayout {
    private Image mImage;
    private Text mText;

    /**
     * WheelItem instance
     * @param context 上下文
     */
    public WheelItem(Context context) {
        this(context, null);
    }

    /**
     * WheelItem instance
     * @param context 上下文
     * @param attrSet attrSet
     */
    public WheelItem(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    /**
     * WheelItem instance
     * @param context 上下文
     * @param attrSet attrSet
     * @param styleName styleName
     */
    public WheelItem(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init();
    }

    /**
     * 初始化
     */
    private void init() {
        DirectionalLayout layout = new DirectionalLayout(getContext());
        DirectionalLayout.LayoutConfig layoutParams = new DirectionalLayout.LayoutConfig(LayoutConfig.MATCH_PARENT,
                WheelUtils.dip2px(getContext(), WheelConstants.WHEEL_ITEM_HEIGHT));
        layout.setOrientation(Component.HORIZONTAL);
        layout.setPadding(WheelConstants.WHEEL_ITEM_PADDING, WheelConstants.WHEEL_ITEM_PADDING,
                WheelConstants
                .WHEEL_ITEM_PADDING, WheelConstants.WHEEL_ITEM_PADDING);
        layout.setAlignment(LayoutAlignment.CENTER);
        addComponent(layout, layoutParams);

        /**
         * 图片
         */
        mImage = new Image(getContext());
        mImage.setTag(WheelConstants.WHEEL_ITEM_IMAGE_TAG);
        mImage.setVisibility(Component.HIDE);
        ComponentContainer.LayoutConfig imageParams = new ComponentContainer.LayoutConfig(LayoutConfig.MATCH_CONTENT,
                LayoutConfig.MATCH_CONTENT);
        imageParams.setMarginRight(WheelConstants.WHEEL_ITEM_MARGIN);
        layout.addComponent(mImage, imageParams);

        /**
         * 文本
         */
        mText = new Text(getContext());
        mText.setTag(WheelConstants.WHEEL_ITEM_TEXT_TAG);
        mText.setTruncationMode(Text.TruncationMode.ELLIPSIS_AT_END);
        mText.setMaxTextLines(1);
        mText.setPaddingForText(false);
        mText.setTextAlignment(TextAlignment.CENTER);
        mText.setTextColor(Color.BLACK);
        ComponentContainer.LayoutConfig textParams = new ComponentContainer.LayoutConfig(LayoutConfig.MATCH_PARENT,
                LayoutConfig.MATCH_PARENT);
        layout.addComponent(mText, textParams);
    }

    /**
     * 设置文本
     *
     * @param text 文本内容
     */
    public void setText(String text) {
        mText.setText(text);
    }

    /**
     * 获取文本对象
     *
     * @return Text 文本对象
     */
    public Text getText() {
        return mText;
    }

    /**
     * 设置图片资源
     *
     * @param resId 图片id
     */
    public void setImage(int resId) {
        mImage.setVisibility(Component.VISIBLE);
        mImage.setPixelMap(resId);
    }
}
