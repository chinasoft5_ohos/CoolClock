/*
 * Copyright (C) 2016 venshine.cn@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wx.wheelview.widget;

import com.wx.wheelview.adapter.ArrayWheelAdapter;
import com.wx.wheelview.common.WheelConstants;
import com.wx.wheelview.util.WheelUtils;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.utils.TextAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * 滚轮对话框
 *
 * @author venshine
 */
public class WheelViewDialog<T> implements Component.ClickedListener {
    private Text mTitle;
    private Component mLine1;
    private Component mLine2;
    private WheelView<T> mWheelView;
    private WheelView.WheelViewStyle mStyle;
    private Text mButton;
    private CommonDialog mDialog;
    private Context mContext;
    private OnDialogItemClickListener mOnDialogItemClickListener;
    private int mSelectedPos;
    private T mSelectedText;

    /**
     * WheelViewDialog instance
     *
     * @param context 上下文
     */
    public WheelViewDialog(Context context) {
        mContext = context;
        init();
    }

    private void init() {
        Optional<Display> display = DisplayManager.getInstance().getDefaultDisplay(mContext);
        Point point = new Point();
        display.get().getSize(point);

        DirectionalLayout layout = new DirectionalLayout(mContext);
        layout.setWidth((int) (point.getPointX() * 0.85f));
        layout.setOrientation(Component.VERTICAL);
        layout.setPadding(WheelUtils.dip2px(mContext, 20), 0, WheelUtils.dip2px(mContext, 20), 0);

        initView();

        DirectionalLayout.LayoutConfig titleParams =
                new DirectionalLayout.LayoutConfig(
                        DirectionalLayout.LayoutConfig.MATCH_PARENT, WheelUtils.dip2px(mContext, 50));
        layout.addComponent(mTitle, titleParams);

        DirectionalLayout.LayoutConfig lineParams =
                new DirectionalLayout.LayoutConfig(
                        DirectionalLayout.LayoutConfig.MATCH_PARENT, WheelUtils.dip2px(mContext, 2));
        layout.addComponent(mLine1, lineParams);

        ComponentContainer.LayoutConfig wheelParams =
                new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT,
                        ComponentContainer.LayoutConfig.MATCH_CONTENT);
        layout.addComponent(mWheelView, wheelParams);

        DirectionalLayout.LayoutConfig line2Params =
                new DirectionalLayout.LayoutConfig(
                        DirectionalLayout.LayoutConfig.MATCH_PARENT, WheelUtils.dip2px(mContext, 1f));
        layout.addComponent(mLine2, line2Params);

        DirectionalLayout.LayoutConfig buttonParams =
                new DirectionalLayout.LayoutConfig(
                        DirectionalLayout.LayoutConfig.MATCH_PARENT, WheelUtils.dip2px(mContext, 45));
        layout.addComponent(mButton, buttonParams);

        mDialog.setSize((int) (point.getPointX() * 0.85f), ComponentContainer.LayoutConfig.MATCH_CONTENT);
        mDialog.setContentCustomComponent(layout);
    }

    private void initView() {
        mDialog = new CommonDialog(mContext);

        mTitle = new Text(mContext);
        mTitle.setTextColor(new Color(WheelConstants.DIALOG_WHEEL_COLOR));
        mTitle.setTextSize(16, Text.TextSizeType.FP);
        mTitle.setTextAlignment(TextAlignment.CENTER);

        mLine1 = new Component(mContext);
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(WheelConstants.DIALOG_WHEEL_COLOR));
        mLine1.setBackground(shapeElement);

        mWheelView = new WheelView(mContext);
        mWheelView.setSkin(WheelView.Skin.Holo);
        mWheelView.setWheelAdapter(new ArrayWheelAdapter(mContext));
        mStyle = new WheelView.WheelViewStyle();
        mStyle.textColor = Color.GRAY.getValue();
        mStyle.selectedTextZoom = 1.2f;
        mWheelView.setStyle(mStyle);

        mWheelView.setOnWheelItemSelectedListener((position, text) -> {
            mSelectedPos = position;
            mSelectedText = text;
        });

        mLine2 = new Component(mContext);
        mLine2.setBackground(shapeElement);

        mButton = new Text(mContext);
        mButton.setTextColor(new Color(WheelConstants.DIALOG_WHEEL_COLOR));
        mButton.setTextSize(12, Text.TextSizeType.FP);
        mButton.setTextAlignment(TextAlignment.CENTER);
        mButton.setClickable(true);
        mButton.setClickedListener(this);
        mButton.setText("OK");
    }

    /**
     * 点击事件
     *
     * @param onDialogItemClickListener
     * @return WheelViewDialog对象
     */
    public WheelViewDialog setOnDialogItemClickListener(
            OnDialogItemClickListener onDialogItemClickListener) {
        mOnDialogItemClickListener = onDialogItemClickListener;
        return this;
    }

    /**
     * 设置dialog外观颜色
     *
     * @param color 颜色
     * @return WheelViewDialog对象
     */
    public WheelViewDialog setDialogStyle(int color) {
        mTitle.setTextColor(new Color(color));
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(color));
        mLine1.setBackground(shapeElement);
        mLine2.setBackground(shapeElement);
        mButton.setTextColor(new Color(color));
        mStyle.selectedTextColor = color;
        mStyle.holoBorderColor = color;
        return this;
    }

    /**
     * 设置标题
     *
     * @param title 标题
     * @return WheelViewDialog对象
     */
    public WheelViewDialog setTitle(String title) {
        mTitle.setText(title);
        return this;
    }

    /**
     * 设置标题颜色
     *
     * @param color 颜色
     * @return WheelViewDialog对象
     */
    public WheelViewDialog setTextColor(int color) {
        mTitle.setTextColor(new Color(color));
        return this;
    }

    /**
     * 设置标题大小
     *
     * @param size 问题大小
     * @return WheelViewDialog对象
     */
    public WheelViewDialog setTextSize(int size) {
        mTitle.setTextSize(size);
        return this;
    }

    /**
     * 设置按钮文本
     *
     * @param text 按钮文本
     * @return WheelViewDialog对象
     */
    public WheelViewDialog setButtonText(String text) {
        mButton.setText(text);
        return this;
    }

    /**
     * 设置按钮文本颜色
     *
     * @param color 按钮颜色
     * @return WheelViewDialog对象
     */
    public WheelViewDialog setButtonColor(int color) {
        mButton.setTextColor(new Color(color));
        return this;
    }

    /**
     * 设置按钮文本尺寸
     *
     * @param size 按钮文字大小
     * @return WheelViewDialog对象
     */
    public WheelViewDialog setButtonSize(int size) {
        mButton.setTextSize(size);
        return this;
    }

    /**
     * 设置数据项显示个数
     *
     * @param count 滚轮个数
     * @return WheelViewDialog对象
     */
    public WheelViewDialog setCount(int count) {
        mWheelView.setWheelSize(count);
        return this;
    }

    /**
     * 设置控件高度
     *
     * @return WheelViewDialog对象
     */
    public WheelViewDialog setHeight() {
        mWheelView.setHeight(mWheelView.getWheelSize() *
                WheelUtils.dip2px(mContext, WheelConstants.WHEEL_ITEM_HEIGHT));
        return this;
    }

    /**
     * 数据项是否循环显示
     *
     * @param loop 是否循环显示
     * @return WheelViewDialog对象
     */
    public WheelViewDialog setLoop(boolean loop) {
        mWheelView.setLoop(loop);
        return this;
    }

    /**
     * 设置数据项显示位置
     *
     * @param selection 滚轮默认选中位置
     * @return WheelViewDialog对象
     */
    public WheelViewDialog setSelection(int selection) {
        mWheelView.setSelection(selection);
        return this;
    }

    /**
     * 设置数据项
     *
     * @param arrays 数据数组
     * @return WheelViewDialog对象
     */
    public WheelViewDialog setItems(T[] arrays) {
        return setItems(Arrays.asList(arrays));
    }

    /**
     * 设置数据项
     *
     * @param list 数据集合
     * @return WheelViewDialog对象
     */
    public WheelViewDialog setItems(List<T> list) {
        mWheelView.setWheelData(list);
        return this;
    }

    /**
     * 显示
     *
     * @return WheelViewDialog对象
     */
    public WheelViewDialog show() {
        if (!mDialog.isShowing()) {
            mDialog.show();
        }
        return this;
    }

    /**
     * 隐藏
     *
     * @return WheelViewDialog对象
     */
    public WheelViewDialog dismiss() {
        if (mDialog.isShowing()) {
            mDialog.hide();
        }
        return this;
    }

    @Override
    public void onClick(Component component) {
        dismiss();
        if (null != mOnDialogItemClickListener) {
            mOnDialogItemClickListener.onItemClick(mSelectedPos, mSelectedText);
        }
    }

    /**
     * OnDialogItemClickListener
     *
     * @param <T> item
     */
    public interface OnDialogItemClickListener<T> {
        /**
         * onItemClick
         *
         * @param position 位置角标
         * @param item item
         */
        void onItemClick(int position, T item);
    }
}
