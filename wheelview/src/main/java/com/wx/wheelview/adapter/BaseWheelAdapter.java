/*
 * Copyright (C) 2016 venshine.cn@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wx.wheelview.adapter;

import com.wx.wheelview.util.WheelUtils;
import com.wx.wheelview.widget.IWheelView;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;

import java.util.List;

/**
 * 滚轮抽象数据适配器
 *
 * @param <T> item
 * @author venshine
 */
public abstract class BaseWheelAdapter<T> extends BaseItemProvider {
    /**
     * 滚轮数据
     */
    protected List<T> mList = null;
    private boolean mLoop = IWheelView.LOOP;
    private int mWheelSize = IWheelView.WHEEL_SIZE;
    private boolean mClickable = IWheelView.CLICKABLE;
    private int mCurrentPositon = -1;
    private OnClickListener mOnClickListener;

    /**
     * abstract bindView
     *
     * @param position 位置角标
     * @param convertView 控件
     * @param parent 父控件
     * @return 控件
     */
    protected abstract Component bindView(int position, Component convertView, ComponentContainer parent);

    /**
     * 设置当前刻度
     *
     * @param position 选中位置
     */
    public final void setCurrentPosition(int position) {
        mCurrentPositon = position;
    }

    /**
     * 设置点击监听
     *
     * @param onClickListener 监听回调
     */
    public void setOnClickListener(OnClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }

    @Override
    public final int getCount() {
        if (mLoop) {
            return Integer.MAX_VALUE;
        }
        return !WheelUtils.isEmpty(mList) ? (mList.size() + mWheelSize - 1) : 0;
    }

    @Override
    public final long getItemId(int position) {
        return !WheelUtils.isEmpty(mList) ? position % mList.size() : position;
    }

    @Override
    public final T getItem(int position) {
        return !WheelUtils.isEmpty(mList) ? mList.get(position % mList.size()) : null;
    }

    /**
     * areAllItemsEnabled
     *
     * @return 是否都Enable
     */
    public boolean areAllItemsEnabled() {
        return !mClickable;
    }

    /**
     * isEnabled
     *
     * @param position 位置角标
     * @return 是否Enable
     */
    public boolean isEnabled(int position) {
        if (mClickable) {
            if (mLoop) {
                if (position % mList.size() == mCurrentPositon) {
                    return true;
                }
            } else {
                if (position == (mCurrentPositon + mWheelSize / 2)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public Component getComponent(int position, Component convertView, ComponentContainer parent) {
        if (mLoop) {
            position = position % mList.size();
        } else {
            if (position < mWheelSize / 2) {
                position = -1;
            } else if (position >= mWheelSize / 2 + mList.size()) {
                position = -1;
            } else {
                position = position - mWheelSize / 2;
            }
        }
        Component view;
        if (position == -1) {
            view = bindView(0, convertView, parent);
        } else {
            view = bindView(position, convertView, parent);
        }
        if (!mLoop) {
            if (position == -1) {
                view.setVisibility(Component.INVISIBLE);
            } else {
                view.setVisibility(Component.VISIBLE);
            }
        }
        if (mOnClickListener != null) {
            final int finalPosition = position;
            view.setClickedListener(v -> mOnClickListener.onPositionClick(finalPosition));
        }
        return view;
    }

    /**
     * 设置可否被点击
     *
     * @param clickable 是否能点击
     * @return BaseWheelAdapter对象
     */
    public final BaseWheelAdapter setClickable(boolean clickable) {
        if (clickable != mClickable) {
            mClickable = clickable;
        }
        return this;
    }

    /**
     * 设置可否循环滚动
     *
     * @param loop 可否循环滚动
     * @return BaseWheelAdapter对象
     */
    public final BaseWheelAdapter setLoop(boolean loop) {
        if (loop != mLoop) {
            mLoop = loop;
        }
        return this;
    }

    /**
     * 设置滚轮个数
     *
     * @param wheelSize 滚轮个数
     * @return BaseWheelAdapter对象
     */
    public final BaseWheelAdapter setWheelSize(int wheelSize) {
        mWheelSize = wheelSize;
        return this;
    }

    /**
     * 设置适配器数据
     *
     * @param list 适配器数据
     * @return BaseWheelAdapter对象
     */
    public final BaseWheelAdapter setData(List<T> list) {
        mList = list;
        return this;
    }

    /**
     * 数据已改变，重绘可见区域
     */
    @Override
    @Deprecated
    public final void notifyDataChanged() {
        super.notifyDataChanged();
    }

    /**
     * OnClickListener监听
     */
    public interface OnClickListener {
        /**
         * onPositionClick
         *
         * @param position 位置
         */
        void onPositionClick(int position);
    }
}
