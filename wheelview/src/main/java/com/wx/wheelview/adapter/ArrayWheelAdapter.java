/*
 * Copyright (C) 2016 venshine.cn@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wx.wheelview.adapter;

import com.wx.wheelview.widget.WheelItem;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.app.Context;

/**
 * 滚轮数组适配器
 *
 * @param <T> item
 * @author venshine
 */
public class ArrayWheelAdapter<T> extends BaseWheelAdapter<T> {
    private Context mContext;

    /**
     * ArrayWheelAdapter instance
     *
     * @param context 上下文
     */
    public ArrayWheelAdapter(Context context) {
        mContext = context;
    }

    @Override
    public Component bindView(int position, Component convertView, ComponentContainer parent) {
        if (convertView == null) {
            convertView = new WheelItem(mContext);
        }
        WheelItem wheelItem = (WheelItem) convertView;
        T item = getItem(position);
        if (item != null) {
            wheelItem.setText(item.toString());
        }
        return convertView;
    }
}
