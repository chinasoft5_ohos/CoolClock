/*
 * Copyright (C) 2016 venshine.cn@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wx.wheelview.adapter;

import com.wx.wheelview.common.WheelData;
import com.wx.wheelview.widget.WheelItem;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.app.Context;

/**
 * 滚轮图片和文本适配器
 *
 * @author venshine
 */
public class SimpleWheelAdapter extends BaseWheelAdapter<WheelData> {
    private Context mContext;

    /**
     * SimpleWheelAdapter instance
     *
     * @param context 上下文
     */
    public SimpleWheelAdapter(Context context) {
        mContext = context;
    }

    @Override
    public Component bindView(int position, Component convertView, ComponentContainer parent) {
        if (convertView == null) {
            convertView = new WheelItem(mContext);
        }
        WheelItem item = (WheelItem) convertView;
        item.setImage(mList.get(position).getId());
        item.setText(mList.get(position).getName());
        return convertView;
    }
}
