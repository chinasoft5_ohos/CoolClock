package clock.socoolby.com.clock;

import clock.socoolby.com.clock.utils.*;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class ExampleOhosTest {
    private final Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();

    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("clock.socoolby.com.clock", actualBundleName);
    }

    @Test
    public void isApplicationBroughtToBackground() {
        boolean applicationBroughtToBackground = ScreenManager.isApplicationBroughtToBackground(context);
        assertFalse(applicationBroughtToBackground);
    }

    @Test
    public void getVersionName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        String versionName = FuncUnit.getVersionName(actualBundleName);
        assertEquals("1.0.1", versionName);
    }


    @Test
    public void isExistsFile() {
        boolean existsFile = FileUtils.isExistsFile(Constants.SHARE_PERFERENCE_FILE);
        System.out.println("file exists:" + existsFile);
    }

    @Test
    public void sharePerferenceModelSave() {
        SharePerferenceModel model = new SharePerferenceModel();
        model.setTypeHourPower(Constants.TALKING_HALF_AN_HOUR);
        DateModel startTimeModel = new DateModel();
        startTimeModel.setTime(12, 31);
        DateModel stopTimeModel = new DateModel();
        stopTimeModel.setTime(14, 31);
        model.setStartHourPowerTime(startTimeModel);
        model.setStopHourPowerTime(stopTimeModel);
        try {
            String city = context.getResourceManager().getElement(ResourceTable.String_shenzhen).getString();
            model.setCity(city);
        } catch (Exception e) {
            e.printStackTrace();
            model.setCity("");
        }
        model.save();
    }

    @Test
    public void readObject() {
        Object o = FileUtils.readObject(Constants.SHARE_PERFERENCE_FILE);
        System.out.println(o);
    }

    @Test
    public void sharePerferenceModelRead() {
        SharePerferenceModel model = new SharePerferenceModel();
        model.read();
        System.out.println(model.toString());
    }
}