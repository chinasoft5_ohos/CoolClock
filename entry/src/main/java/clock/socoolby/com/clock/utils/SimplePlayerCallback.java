package clock.socoolby.com.clock.utils;

import clock.socoolby.com.clock.LogUtil;
import ohos.media.player.Player;
import ohos.powermanager.PowerManager;

public class SimplePlayerCallback implements Player.IPlayerCallback {
    @Override
    public void onPrepared() {
        LogUtil.loge("=================>onPrepared");
    }

    @Override
    public void onMessage(int i, int i1) {
        LogUtil.loge("=================>onMessage");

    }

    @Override
    public void onError(int i, int i1) {
        LogUtil.loge("=================>onError");

    }

    @Override
    public void onResolutionChanged(int i, int i1) {
        LogUtil.loge("=================>onResolutionChanged");

    }

    @Override
    public void onPlayBackComplete() {
        LogUtil.loge("=================>onPlayBackComplete");
    }

    @Override
    public void onRewindToComplete() {
        LogUtil.loge("=================>onRewindToComplete");
    }

    @Override
    public void onBufferingChange(int i) {
        LogUtil.loge("=================>onBufferingChange");
    }

    @Override
    public void onNewTimedMetaData(Player.MediaTimedMetaData mediaTimedMetaData) {
        LogUtil.loge("=================>onNewTimedMetaData");
    }

    @Override
    public void onMediaTimeIncontinuity(Player.MediaTimeInfo mediaTimeInfo) {
        LogUtil.loge("=================>onMediaTimeIncontinuity");
    }
}
