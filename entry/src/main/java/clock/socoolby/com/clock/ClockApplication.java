package clock.socoolby.com.clock;

import clock.socoolby.com.clock.utils.Constants;
import clock.socoolby.com.clock.utils.DateModel;
import clock.socoolby.com.clock.utils.FileUtils;
import clock.socoolby.com.clock.utils.SharePerferenceModel;
import ohos.aafwk.ability.AbilityPackage;

public class ClockApplication extends AbilityPackage {
    private static ClockApplication sEndzoneBoxApp;

    public static void initContext(ClockApplication app) {
        sEndzoneBoxApp = app;
    }

    public static ClockApplication getInstance() {
        return sEndzoneBoxApp;
    }

    @Override
    public void onInitialize() {
        super.onInitialize();
        initContext(this);
        if (!FileUtils.isExistsFile(Constants.SHARE_PERFERENCE_FILE)) {
            SharePerferenceModel model = new SharePerferenceModel();
            model.setTypeHourPower(Constants.TALKING_HALF_AN_HOUR);
            DateModel startTimeModel = new DateModel();
            startTimeModel.setTime(12, 31);
            DateModel stopTimeModel = new DateModel();
            stopTimeModel.setTime(14, 31);
            model.setStartHourPowerTime(startTimeModel);
            model.setStopHourPowerTime(stopTimeModel);
            try {
                String city = getResourceManager().getElement(ResourceTable.String_shenzhen).getString();
                model.setCity(city);
            } catch (Exception e) {
                e.printStackTrace();
                model.setCity("");
            }
            model.save();
        }
    }
}
