/*
 * Copyright (C) 2016 venshine.cn@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package clock.socoolby.com.clock;

import com.wx.wheelview.adapter.BaseWheelAdapter;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.app.Context;

/**
 * Demo
 *
 * @author venshine
 */
public class MyWheelAdapter extends BaseWheelAdapter<String> {
    private Context mContext;

    /**
     * MyWheelAdapter instance
     *
     * @param context 上下文
     */
    MyWheelAdapter(Context context) {
        mContext = context;
    }

    @Override
    protected Component bindView(int position, Component convertView, ComponentContainer parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_item_list, null, false);
            viewHolder.textView = (Text) convertView.findComponentById(ResourceTable.Id_item_name);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.textView.setText(mList.get(position));
        return convertView;
    }

    /**
     * ViewHolder Holder
     */
    static class ViewHolder {
        Text textView;
    }
}
