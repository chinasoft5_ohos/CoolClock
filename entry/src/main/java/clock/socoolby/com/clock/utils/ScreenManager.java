package clock.socoolby.com.clock.utils;


import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilityMissionInfo;
import ohos.agp.utils.TextTool;
import ohos.app.Context;
import ohos.app.IAbilityManager;
import ohos.bundle.ElementName;
import ohos.powermanager.PowerManager;

import java.util.List;

public class ScreenManager {
    public static boolean isScreenOn() {
        PowerManager powerManager = new PowerManager();
        return powerManager.isScreenOn();
    }
    public static boolean isApplicationBroughtToBackground(final Context context) {
        try {
            IAbilityManager abilityManager = context.getAbilityManager();
            List<AbilityMissionInfo> abilityMissionInfos = abilityManager.queryRunningAbilityMissionInfo(1);
            if (!abilityMissionInfos.isEmpty()) {
                ElementName abilityTopBundleName = abilityMissionInfos.get(0).getAbilityTopBundleName();
                if (!TextTool.isEqual(abilityTopBundleName.getBundleName(), context.getBundleName())) {
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            return true;
        }

    }
}
