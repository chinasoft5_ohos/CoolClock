package clock.socoolby.com.clock;

import java.util.logging.Level;
import java.util.logging.Logger;

public class LogUtil {
    private static Logger logger;

    private static Logger createLogger(String tag) {
        if (logger == null) {
            logger = Logger.getLogger(tag);
        }
        return logger;
    }

    public static void loge(String msg) {
        createLogger("log").log(Level.SEVERE, msg);
    }

    public static void loge(String tag, String msg) {
        createLogger(tag).log(Level.SEVERE, msg);
    }
}
