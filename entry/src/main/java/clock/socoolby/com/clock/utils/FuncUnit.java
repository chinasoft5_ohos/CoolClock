package clock.socoolby.com.clock.utils;


import clock.socoolby.com.clock.ClockApplication;
import ohos.bundle.BundleInfo;
import ohos.rpc.RemoteException;

/**
 * Alway zuo,never die.
 * Created by socoolby on 03/01/2017.
 */

public class FuncUnit {


    /**
     * get versionName
     *
     * @param packageName
     * @return versionName
     */
    public static String getVersionName(String packageName) {
        try {
            BundleInfo bundleInfo = ClockApplication.getInstance().getBundleManager().getBundleInfo(packageName, 0);
            return bundleInfo.getVersionName();
        } catch (RemoteException e) {
            return null;
        }
    }


}
