package clock.socoolby.com.clock;

import clock.socoolby.com.clock.utils.FuncUnit;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Text;
import ohos.agp.text.RichText;
import ohos.agp.text.RichTextBuilder;
import ohos.agp.text.TextForm;
import ohos.agp.utils.Color;
import ohos.utils.net.Uri;

public class AboutAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        getWindow().setNavigationBarColor(Color.BLACK.getValue());
        super.setUIContent(ResourceTable.Layout_ability_about);
        Text github = (Text) findComponentById(ResourceTable.Id_github);
        Text version = (Text) findComponentById(ResourceTable.Id_version);
        try {
            String versionStr = getResourceManager().getElement(ResourceTable.String_version).getString();
            version.setText(versionStr + FuncUnit.getVersionName(getBundleName()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        RichText leadingMargin = new RichTextBuilder()
                .mergeForm(new TextForm()
                        .setUnderline(true)
                        .setTextColor(github.getTextColor().getValue())
                        .setTextSize(github.getTextSize())
                )
                .addText(github.getText())
                .build();
        github.postLayout();
        github.setRichText(leadingMargin);
    }


    private void openBrowser(String url) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        Uri uri = Uri.parse(url);
        intent.setUri(uri);
        startAbility(intent);
    }
}
