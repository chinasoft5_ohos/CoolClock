package clock.socoolby.com.clock;

import clock.socoolby.com.clock.utils.Constants;
import clock.socoolby.com.clock.utils.DateModel;
import clock.socoolby.com.clock.utils.SharePerferenceModel;
import com.wx.wheelview.adapter.ArrayWheelAdapter;
import com.wx.wheelview.common.WheelConstants;
import com.wx.wheelview.util.WheelUtils;
import com.wx.wheelview.widget.WheelView;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.utils.net.Uri;

import java.util.Arrays;

public class SettingAbility extends Ability implements Component.ClickedListener {

    private SharePerferenceModel model;
    private String[] listTime = new String[48];
    private WheelView weelStartTime, weelStopTime;
    private RadioButton rbHalfhour, rbHours, rbNoreport;
    private TextField etCity, etDescription;
    private Checkbox cbTick, cbTriggerScreen;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        getWindow().setNavigationBarColor(Color.BLACK.getValue());
        super.setUIContent(ResourceTable.Layout_ability_setting);
        weelStartTime = (WheelView) findComponentById(ResourceTable.Id_weel_start_time);
        weelStopTime = (WheelView) findComponentById(ResourceTable.Id_weel_stop_time);
        rbHalfhour = (RadioButton) findComponentById(ResourceTable.Id_rb_halfhour);
        rbHours = (RadioButton) findComponentById(ResourceTable.Id_rb_hours);
        rbNoreport = (RadioButton) findComponentById(ResourceTable.Id_rb_noreport);
        etCity = (TextField) findComponentById(ResourceTable.Id_et_city);
        etDescription = (TextField) findComponentById(ResourceTable.Id_et_description);
        cbTick = (Checkbox) findComponentById(ResourceTable.Id_cb_tick);
        cbTriggerScreen = (Checkbox) findComponentById(ResourceTable.Id_cb_trigger_screen);
        RadioContainer rgTalkingClock = (RadioContainer) findComponentById(ResourceTable.Id_rg_talking_clock);


        model = new SharePerferenceModel();
        model.read();

        cbTick.setChecked(model.isTickSound());
        cbTick.setCheckedStateChangedListener((absButton, b) -> model.setTickSound(b));
        cbTriggerScreen.setChecked(model.isTriggerScreen());
        cbTriggerScreen.setCheckedStateChangedListener((absButton, b) -> model.setTriggerScreen(b));

        Button btnUninstall = (Button) findComponentById(ResourceTable.Id_btn_uninstall);
        Button btnAbout = (Button) findComponentById(ResourceTable.Id_btn_about);
        btnAbout.setClickedListener(this);
        btnUninstall.setClickedListener(this);

        initWeelTime();
        initData();
        rgTalkingClock.setMarkChangedListener((radioContainer, i) -> {
            int markedButtonId = radioContainer.getMarkedButtonId();
            switch (markedButtonId) {
                case 0:
                    model.setTypeHourPower(Constants.TALKING_HALF_AN_HOUR);
                    break;
                case 1:
                    model.setTypeHourPower(Constants.TALKING_HOURS);
                    break;
                case 2:
                    model.setTypeHourPower(Constants.TALKING_NO_REPORT);
                    break;
                default:
                    break;
            }
        });
    }

    private void initWeelTime() {
        for (int i = 0; i < 48; i++) {
            int hours = i / 2;
            int minutes = i % 2 * 30;
            String timeString = String.format("%02d:%02d", hours, (minutes + 1));
            listTime[i] = timeString;
        }

        int startTimeIndex = indexOfTimeString(String.format("%02d:%02d", model.getStartHourPowerTime().getHour(), model.getStartHourPowerTime().getMinute()));
        int stopTimeIndex = indexOfTimeString(String.format("%02d:%02d", model.getStopHourPowerTime().getHour(), model.getStopHourPowerTime().getMinute()));


        WheelView.WheelViewStyle style = new WheelView.WheelViewStyle();
        style.backgroundColor = Color.getIntColor("#d8d8d8");
        style.textColor = Color.getIntColor("#000000");
        style.selectedTextColor = Color.getIntColor("#717171");
        style.textSize = 16;

        weelStartTime.setWheelAdapter(new MyWheelAdapter(this));
        weelStartTime.setWheelSize(5);
        weelStartTime.setSelection(startTimeIndex);
        weelStartTime.setClickToPosition(true);
        weelStartTime.setHeight(weelStartTime.getWheelSize() * WheelUtils.dip2px(getContext(), 28));
        weelStartTime.setSkin(WheelView.Skin.Common);
        weelStartTime.setStyle(style);
        weelStartTime.setWheelData(Arrays.asList(listTime.clone()));


        weelStopTime.setWheelAdapter(new MyWheelAdapter(this));
        weelStopTime.setWheelSize(5);
        weelStopTime.setSelection(stopTimeIndex);
        weelStopTime.setClickToPosition(true);
        weelStopTime.setHeight(weelStopTime.getWheelSize() * WheelUtils.dip2px(getContext(), 28));
        weelStopTime.setSkin(WheelView.Skin.Common);
        weelStopTime.setStyle(style);
        weelStopTime.setWheelData(Arrays.asList(listTime.clone()));
    }


    private void initData() {
        switch (model.getTypeHourPower()) {
            case Constants.TALKING_HALF_AN_HOUR:
                rbHalfhour.setChecked(true);
                break;
            case Constants.TALKING_HOURS:
                rbHours.setChecked(true);
                break;
            case Constants.TALKING_NO_REPORT:
                rbNoreport.setChecked(true);
                break;
        }
        etCity.setText(model.getCity());
        etDescription.setText(model.getDescription());
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        reportTimeConfirm();
        model.setCity(etCity.getText());
        model.setDescription(etDescription.getText());
        model.save();
        setResult(Constants.SUCCESS_CODE, new Intent());
        terminateAbility();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    private void reportTimeConfirm() {
        String timeStart = listTime[weelStartTime.getSelection()];
        String timeStop = listTime[weelStopTime.getSelection()];

        DateModel startTimeModel = new DateModel();
        startTimeModel.setTimeString(timeStart);
        DateModel stopTimeModel = new DateModel();
        stopTimeModel.setTimeString(timeStop);
        model.setStartHourPowerTime(startTimeModel);
        model.setStopHourPowerTime(stopTimeModel);
    }


    private int indexOfTimeString(String timeString) {
        for (int i = listTime.length - 1; i >= 0; i--) {
            if (listTime[i].equals(timeString))
                return i;
        }
        return 0;
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_btn_about:
                openAboutAbility();
                break;
            case ResourceTable.Id_btn_uninstall:
                break;
            default:
                break;
        }
    }

    private void uninstall() {
        Uri packageUri = Uri.parse("package:" + getBundleName());//包名，指定该应用
        LogUtil.loge("=================>" + getBundleName());
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_BUNDLE_REMOVE);
        intent.setUri(packageUri);
        startAbility(intent);


    }


    private void openAboutAbility() {
        Intent intent = new Intent();
        Operation opt = new Intent
                .OperationBuilder()
                .withBundleName(getBundleName())
                .withAbilityName(AboutAbility.class.getCanonicalName())
                .build();
        intent.setOperation(opt);
        startAbility(intent);
    }
}
