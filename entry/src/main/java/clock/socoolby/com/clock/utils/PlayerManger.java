package clock.socoolby.com.clock.utils;


import clock.socoolby.com.clock.ClockApplication;
import clock.socoolby.com.clock.LogUtil;
import ohos.app.Context;
import ohos.global.resource.RawFileDescriptor;
import ohos.global.resource.RawFileEntry;
import ohos.media.audio.AudioManager;
import ohos.media.player.Player;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;

public class PlayerManger {
    private static final float BEEP_VOLUME = 1.0f;
    private Player mediaPlayer;
    private LinkedList<String> playList;
    private static PlayerManger playerMangerInstance;
    private HashMap<Integer, String> NUM_AUDIO = new HashMap();
    private HashMap<Integer, String> WEEK_AUDIO = new HashMap();

    private boolean isReporttime = false;
    private RawFileDescriptor tickFile = null;

    public static PlayerManger getInstance() {
        if (playerMangerInstance == null) {
            synchronized (PlayerManger.class) {
                if (playerMangerInstance == null) {
                    init();
                }
            }
        }

        return playerMangerInstance;
    }

    private static void init() {
        playerMangerInstance = new PlayerManger();
        playerMangerInstance.NUM_AUDIO.put(0, "resources/rawfile/n0.mp3");
        playerMangerInstance.NUM_AUDIO.put(1, "resources/rawfile/n1.mp3");
        playerMangerInstance.NUM_AUDIO.put(2, "resources/rawfile/n2.mp3");
        playerMangerInstance.NUM_AUDIO.put(3, "resources/rawfile/n3.mp3");
        playerMangerInstance.NUM_AUDIO.put(4, "resources/rawfile/n4.mp3");
        playerMangerInstance.NUM_AUDIO.put(5, "resources/rawfile/n5.mp3");
        playerMangerInstance.NUM_AUDIO.put(6, "resources/rawfile/n6.mp3");
        playerMangerInstance.NUM_AUDIO.put(7, "resources/rawfile/n7.mp3");
        playerMangerInstance.NUM_AUDIO.put(8, "resources/rawfile/n8.mp3");
        playerMangerInstance.NUM_AUDIO.put(9, "resources/rawfile/n9.mp3");
        playerMangerInstance.NUM_AUDIO.put(10, "resources/rawfile/n10.mp3");
        playerMangerInstance.NUM_AUDIO.put(20, "resources/rawfile/n20.mp3");
        playerMangerInstance.NUM_AUDIO.put(30, "resources/rawfile/n30.mp3");
        playerMangerInstance.NUM_AUDIO.put(40, "resources/rawfile/n40.mp3");
        playerMangerInstance.NUM_AUDIO.put(50, "resources/rawfile/n50.mp3");
        playerMangerInstance.NUM_AUDIO.put(60, "resources/rawfile/n60.mp3");

        playerMangerInstance.WEEK_AUDIO.put(0, "resources/rawfile/sunday.mp3");
        playerMangerInstance.WEEK_AUDIO.put(1, "resources/rawfile/monday.mp3");
        playerMangerInstance.WEEK_AUDIO.put(2, "resources/rawfile/tuesday.mp3");
        playerMangerInstance.WEEK_AUDIO.put(3, "resources/rawfile/wednesday.mp3");
        playerMangerInstance.WEEK_AUDIO.put(4, "resources/rawfile/thursday.mp3");
        playerMangerInstance.WEEK_AUDIO.put(5, "resources/rawfile/friday.mp3");
        playerMangerInstance.WEEK_AUDIO.put(6, "resources/rawfile/saturday.mp3");

    }


    private void play(final Context context) {
        if (playList.size() == 0) {
            isReporttime = false;
            return;
        }

        isReporttime = true;
        String resourceID = playList.get(0);
        playList.remove(0);
        RawFileEntry rawFileEntry = context.getResourceManager().getRawFileEntry(resourceID);
        RawFileDescriptor rawFileDescriptor = null;
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
        mediaPlayer = buildMediaPlayer();
        try {
            rawFileDescriptor = rawFileEntry.openRawFileDescriptor();
            mediaPlayer.reset();
            mediaPlayer.setSource(rawFileDescriptor);
            mediaPlayer.setVolume(BEEP_VOLUME);
            mediaPlayer.prepare();
            mediaPlayer.setPlayerCallback(new SimplePlayerCallback() {
                @Override
                public void onPlayBackComplete() {
                    super.onPlayBackComplete();
                    play(context);
                }

                @Override
                public void onRewindToComplete() {
                    super.onRewindToComplete();
                }

                @Override
                public void onError(int errorType, int errorCode) {
                    super.onError(errorType, errorCode);

                }
            });
            mediaPlayer.play();
        } catch (IOException ioe) {
            mediaPlayer = null;
        } finally {
            if (rawFileDescriptor != null) {
                try {
                    rawFileDescriptor.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public void reportTime(Context context, int year, int month, int day, int hour, int minute, int today) {
        if (isReporttime) {
            return;

        }
        LogUtil.loge(String.format("reportTime Year:%d Month:%d Day:%d Hour:%d Minute:%d  Today:%d", year, month, day, hour, minute, today));
        LinkedList<String> playList = new LinkedList<String>();
        playList.add("resources/rawfile/todayis.mp3");

        int monthTenDigit = month / 10 * 10;
        int monthUnit = month % 10;
        if (monthTenDigit >= 10)
            playList.add(NUM_AUDIO.get(monthTenDigit));
        if (monthUnit > 0)
            playList.add(NUM_AUDIO.get(monthUnit));
        playList.add("resources/rawfile/month.mp3");

        int dayTenDigit = day / 10 * 10;
        int dayUnit = day % 10;
        if (dayTenDigit >= 10)
            playList.add(NUM_AUDIO.get(dayTenDigit));
        if (dayUnit > 0)
            playList.add(NUM_AUDIO.get(dayUnit));
        playList.add("resources/rawfile/day.mp3");

        playList.add(WEEK_AUDIO.get(today));


        int hourUnit;
        if (hour >= 20) {
            playList.add("resources/rawfile/n20.mp3");
            hourUnit = hour % 20;
            if (hourUnit > 0) {
                playList.add(NUM_AUDIO.get(hourUnit));
            }
        } else if (hour >= 10) {
            playList.add("resources/rawfile/n10.mp3");
            hourUnit = hour % 10;
            if (hourUnit > 0) {
                playList.add(NUM_AUDIO.get(hourUnit));
            }
        } else {
            playList.add(NUM_AUDIO.get(hour));
        }
        playList.add("resources/rawfile/hour.mp3");

        int minuteUnit = minute % 10;
        int minuteTenDigit = minute / 10 * 10;
        playList.add(NUM_AUDIO.get(minuteTenDigit));
        if (minuteUnit > 0)
            playList.add(NUM_AUDIO.get(minuteUnit));
        playList.add("resources/rawfile/minute.mp3");
        play(playList, context);
    }

    private void play(LinkedList<String> playList, Context context) {
        this.playList = playList;

        play(context);
    }


    public void playTick(Context context) {
        if (!ScreenManager.isScreenOn() || ScreenManager.isApplicationBroughtToBackground(context)) {
            return;
        }
        if (!isReporttime) {
            if (mediaPlayer != null) {
                mediaPlayer.release();
                mediaPlayer = null;
            }
            mediaPlayer = buildMediaPlayer();
            try {
                if (tickFile == null) {
                    tickFile = context.getResourceManager().getRawFileEntry("resources/rawfile/tick.mp3").openRawFileDescriptor();
                }
                mediaPlayer.reset();
                mediaPlayer.setSource(tickFile);
                mediaPlayer.setVolume(BEEP_VOLUME);
                mediaPlayer.prepare();
//                mediaPlayer.setPlayerCallback(new SimplePlayerCallback() {
//                    @Override
//                    public void onPlayBackComplete() {
//                        super.onPlayBackComplete();
//
//                    }
//                });
                mediaPlayer.play();
            } catch (IOException ioe) {
                mediaPlayer = null;
            }
        }

    }

    private static Player buildMediaPlayer() {
        Player player = new Player(ClockApplication.getInstance());
        player.setAudioStreamType(AudioManager.AudioVolumeType.STREAM_MUSIC.getValue());
        return player;
    }


}
